import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-guess',
  templateUrl: './guess.component.html',
  styleUrls: ['./guess.component.scss'],
})
export class GuessComponent implements OnInit {
  @Input() letter!: string;
  @Input() blank!: boolean;
  @Input() isAnswer!: boolean;

  constructor() {}

  ngOnInit(): void {}
}
