import { environment } from 'src/environments/environment';
import { Component, HostListener, OnInit } from '@angular/core';

import { LetterService } from './../../services/letter.service';

@Component({
  selector: 'app-guesses',
  templateUrl: './guesses.component.html',
  styleUrls: ['./guesses.component.scss'],
})
export class GuessesComponent implements OnInit {
  sequence: string = '';
  blocks: string[] = [];
  answer: string = this.letterService.answer;

  constructor(private letterService: LetterService) {}

  ngOnInit(): void {
    this.letterService.letterEvent.subscribe((letter) => {
      this.updateBlocks(letter);
      if (
        letter === this.answer ||
        this.blocks.length >= environment.MAX_TRIES
      ) {
        this.letterService.winEvent.emit(this.blocks.length);
      }
    });
    this.letterService.restartEvent.subscribe(() => {
      this.reset();
    });
    this.updateBlocks();
  }

  updateBlocks(letter?: string): void {
    if (letter != undefined) this.sequence += letter;
    this.blocks = this.buildArray(this.sequence);

    // Scroll to bottom of the page
    setTimeout(() => {
      window.scrollTo(0, document.body.scrollHeight);
    }, 0);
  }

  buildArray(model: string): string[] {
    var arr: string[] = [];
    for (const s of model) arr.push(s);
    return arr;
  }

  reset() {
    this.sequence = '';
    this.blocks = [];
    this.answer = this.letterService.answer;
  }
}
