import { Component, OnInit } from '@angular/core';
import { LetterService } from 'src/app/services/letter.service';

import { environment } from './../../../environments/environment';

@Component({
  selector: 'app-keyboard',
  templateUrl: './keyboard.component.html',
  styleUrls: ['./keyboard.component.scss'],
})
export class KeyboardComponent implements OnInit {
  keyboardLayout: string[][] = [];
  answer: string = '';

  constructor(private letterService: LetterService) {}

  ngOnInit(): void {
    this.reset();
    this.keyboardLayout = this.buildSequence('qwerty');
    this.letterService.restartEvent.subscribe(() => {
      this.reset();
    });
  }

  buildSequence(layoutName: string): string[][] {
    var lay = '';
    var pos = [];

    switch (layoutName.toLowerCase()) {
      case 'qwerty':
        lay = environment.QWERTY_KLayout;
        pos = environment.QWERTY_stopPos;
        break;
      case 'dvorak':
        lay = environment.DVORAK_KLayout;
        pos = environment.DVORAK_stopPos;
        break;
      default:
        console.error('Layout "' + layoutName + '" não reconhecido!');
        return [];
    }

    var seq: string[][] = [];
    for (let i = 0; i < pos.length; i++) {
      if (i === 0) {
        seq.push(lay.slice(0, pos[i]).split(''));
      } else {
        seq.push(lay.slice(pos[i - 1], pos[i]).split(''));
      }
      if (pos[i] < lay.length && i === pos.length - 1) {
        seq.push(lay.slice(pos[i]).split(''));
      }
    }

    return seq;
  }

  reset() {
    this.answer = this.letterService.answer;
    // console.log('Resposta: ' + this.answer.toUpperCase());
  }

}
