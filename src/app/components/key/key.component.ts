import { Component, HostListener, Input, OnInit } from '@angular/core';

import { LetterService } from './../../services/letter.service';

@Component({
  selector: 'app-key',
  templateUrl: './key.component.html',
  styleUrls: ['./key.component.scss'],
})
export class KeyComponent implements OnInit {
  @Input() mykey!: string;
  @Input() isAnswer!: boolean;

  touched: boolean = false;

  constructor(private letterService: LetterService) {}

  ngOnInit(): void {
    this.letterService.restartEvent.subscribe(() => {
      this.reset();
    });
  }

  setTouched() {
    if (!this.letterService.gameWon) {
      this.letterService.letterEvent.emit(this.mykey);
      this.touched = true;
    }
  }

  reset() {
    this.touched = false;
  }

  @HostListener('window:keyup', ['$event'])
  keyEvent(event: KeyboardEvent) {
    if (event.key == this.mykey) this.setTouched();
  }
}
