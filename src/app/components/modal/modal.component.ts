import { environment } from 'src/environments/environment';
import { LetterService } from 'src/app/services/letter.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss'],
})
export class ModalComponent implements OnInit {
  gameWon: string = 'hidden';
  sharing: string = 'hidden';

  shareTimerMs: number = 1500;

  modalMessage: string = '';

  whiteSquare: string = '⬜';
  greenSquare: string = '🟩';

  siteUrl: string = 'angeloleite-letterle.netlify.app';

  tries: number = 0;

  constructor(private letterService: LetterService) {
    this.letterService.winEvent.subscribe((tries) => {
      this.modalMessage =
        tries >= environment.MAX_TRIES
          ? "You somehow didn't get the correct letter after " +
            environment.MAX_TRIES +
            ' guesses.'
          : 'You got the correct letter in ' + tries + ' guesses.';

      this.tries = tries;
      this.gameWon = 'visible';
    });
    this.letterService.restartEvent.subscribe(() => {
      this.reset();
    });
  }

  ngOnInit(): void {}

  share() {
    this.sharing = 'visible';
    setTimeout(() => {
      this.sharing = 'hidden';
    }, this.shareTimerMs);

    var message = 'Letterle ' + this.tries + '/26 ';
    for (let i = 0; i < this.tries - 1; i++) message += this.whiteSquare + ' ';

    if (this.tries < environment.MAX_TRIES) message += this.greenSquare + ' ';

    message += this.siteUrl;

    navigator.clipboard.writeText(message);
  }

  reset() {
    this.tries = 0;
    this.gameWon = 'hidden';
    this.sharing = 'hidden';
  }
}
