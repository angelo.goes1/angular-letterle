import { EventEmitter, Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class LetterService {
  alphabet: string[] = environment.QWERTY_KLayout.split('');
  answer: string =
    this.alphabet[Math.floor(Math.random() * this.alphabet.length)];
  gameWon: boolean = false;

  letterEvent: EventEmitter<string> = new EventEmitter();
  winEvent: EventEmitter<any> = new EventEmitter();
  loseEvent: EventEmitter<any> = new EventEmitter();
  restartEvent: EventEmitter<any> = new EventEmitter();

  constructor() {}
  ngOnInit(): void {}

  reset() {
    this.answer =
      this.alphabet[Math.floor(Math.random() * this.alphabet.length)];
  }
}
