import { Component } from '@angular/core';
import { LetterService } from './services/letter.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  gameWon: string = 'hidden';

  constructor(private letterService: LetterService) {
    this.letterService.winEvent.subscribe(() => {
      this.gameWon = 'visible';
      this.letterService.gameWon = true;
    });
    this.letterService.restartEvent.subscribe(() => {
      this.gameWon = 'hidden';
      this.letterService.gameWon = false;
    });
  }
  title = 'letterle';

  restart() {
    console.log('Recomeçando jogo...');
    this.letterService.reset();
    this.letterService.restartEvent.emit();
  }
}
