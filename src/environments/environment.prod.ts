export const environment = {
  production: true,
  QWERTY_KLayout: 'qwertyuiopasdfghjklzxcvbnm',
  QWERTY_stopPos: [10, 19],
  DVORAK_KLayout: 'pyfgcrlaoeuidhtnsqjkxbmwvz',
  DVORAK_stopPos: [7, 17],
  MAX_TRIES: 26,
};
